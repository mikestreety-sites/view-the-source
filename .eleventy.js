module.exports = function (config) {
    // config.addFilter('log', require('./app/filters/log.js'));

    return {
        dir: {
            input: 'app/content',
            output: 'html',

            data: './../data',
            includes: './../includes',
            layouts: './../layouts'
        }
    };
};
